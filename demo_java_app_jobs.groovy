import utils.JobUtil

def config = [
    context: this,
	APP_NAME: "demo-java-app",
	GIT_REPO: "https://gitlab.com/kchaganti/demo-java-app.git"
]

JobUtil.buildPipeline(pipelineJob("${config.APP_NAME}-pipeline"), config)
JobUtil.deploymentPipeline(pipelineJob("${config.APP_NAME}-deploy"), config)
