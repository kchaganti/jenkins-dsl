package utils

import utils.Properties
import javaposse.jobdsl.dsl.Job

class JobUtil {

	static Job buildPipeline(job, args) {
		return job.with {
			definition {
				cpsScm {
					scm {
						git {
							remote {
								url("${args.GIT_REPO}")
							}
							branch("refs/heads/ci-pipeline")
							extensions {
								preBuildMerge {
									options {
										mergeRemote('origin')
										mergeTarget('master')
										mergeStrategy('DEFAULT')
										fastForwardMode('FF')
									}
								}
							}
						}
					}
					scriptPath("jenkins/Jenkinsfile-Pipeline")
				}
			}
			triggers {
				scm('* * * * *')
			}
			disabled(false)
		}
	}

	static Job deploymentPipeline(job, args) {
		return job.with {
			parameters {
				stringParam("VERSION", "", "The version to be deployed. If blank, the most recently tagged version will be deployed.")
				choiceParam("DEPLOY_ENV", ["devops"], "")
			}
			definition {
				cpsScm {
					scm {
						git {
							remote {
								url("${args.GIT_REPO}")
							}
							branch("refs/heads/master")
						}
					}
					scriptPath("jenkins/Jenkinsfile-Deploy")
				}
			}
			triggers {
				scm('* * * * *')
			}
			disabled(false)
		}
	}

}