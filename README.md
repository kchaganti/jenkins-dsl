# Jenkins Domain Specific Language (DSL)

## What is in this repo?
This repo contains code-based Jenkins job definitions that are defined using Jenkins DSL. Any application
that uses Jenkins jobs defines those jobs in the root directory of this repo.

## How can I add jobs for a new application?
Create a .groovy file in the root directory of this repo. Name is something meaningful (i.e. my_app.groovy).   
In that file, define the jobs that you want to create using the code samples from other files in the repo.

## Where can I get more details about the DSL API?
The Jenkins DSL Plugin GitHub page is a good place to start ([here](https://github.com/jenkinsci/job-dsl-plugin)).   
For specific API documentation, see this API reference ([here](https://jenkinsci.github.io/job-dsl-plugin/)).